/**
 * 
 */
package com.jeesuite.bestpl.message;

import java.io.Serializable;

import com.jeesuite.kafka.handler.MessageHandler;
import com.jeesuite.kafka.message.DefaultMessage;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月25日
 */
public class UserScoreMessageHandler implements MessageHandler {


	@Override
	public void p1Process(DefaultMessage message) {
		//TODO 
	}

	@Override
	public void p2Process(DefaultMessage message) {
		Serializable body = message.getBody();
		System.out.println("UserScoreMessageHandler process message:" + body);
	}


	@Override
	public boolean onProcessError(DefaultMessage message) {
		return true;
	}

}
