/**
 * 
 */
package com.jeesuite.bestpl.task;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.jeesuite.bestpl.dao.entity.UserScoreEntity;
import com.jeesuite.bestpl.dao.entity.UserScoreLogEntity;
import com.jeesuite.bestpl.dao.mapper.UserScoreEntityMapper;
import com.jeesuite.bestpl.dao.mapper.UserScoreLogEntityMapper;
import com.jeesuite.scheduler.AbstractJob;
import com.jeesuite.scheduler.JobContext;

/**
 * @description <br>
 * @author <a href="mailto:wei.jiang@lifesense.com">vakin</a>
 * @date 2016年1月28日
 * @Copyright (c) 2015, lifesense.com
 */
public class UserScoreCheckTask extends AbstractJob{
	
	private @Autowired UserScoreEntityMapper userScoreMapper;
	private @Autowired UserScoreLogEntityMapper userScoreLogMapper;

	@Override
	public void doJob(JobContext context) throws Exception {
		List<UserScoreEntity> list = userScoreMapper.selectAll();
		for (UserScoreEntity userScoreEntity : list) {			
			UserScoreLogEntity logEntity = userScoreLogMapper.findLastByUsertId(userScoreEntity.getUserId());
			//TODO do check
		}
	}

	@Override
	public boolean parallelEnabled() {
		return false;
	}

}
