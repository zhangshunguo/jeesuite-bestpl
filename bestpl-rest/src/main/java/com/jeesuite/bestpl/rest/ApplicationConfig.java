/**
 * 
 */
package com.jeesuite.bestpl.rest;

import com.jeesuite.bestpl.exception.DemoBaseException;
import com.jeesuite.rest.BaseApplicaionConfig;
import com.jeesuite.rest.excetion.ExcetionWrapper;
import com.jeesuite.rest.response.WrapperResponseEntity;


public class ApplicationConfig extends BaseApplicaionConfig {

	

	public ApplicationConfig() {
		super();
		//register(new JavaxWsrsApiKeeperFilter().register(new AuthCheckHander()));
	}


	@Override
	public ExcetionWrapper createExcetionWrapper() {
		return new bestplExcetionWrapper();
	}


	@Override
	public String packages() {
		return "com.jeesuite.bestpl.rest";
	}
	
	public static class bestplExcetionWrapper implements ExcetionWrapper{

		@Override
		public WrapperResponseEntity toResponse(Exception e) {
			if(e instanceof DemoBaseException){
				DemoBaseException ex = (DemoBaseException) e;
				return new WrapperResponseEntity(ex.getCode() + "", ex.getMessage(), true);
			}
			// 
			return null;
		}
		
	}
	
}
