/**
 * 
 */
package com.jeesuite.bestpl.rest.controller;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.jeesuite.bestpl.api.IUserScoreService;
import com.jeesuite.bestpl.dto.UserScore;
import com.jeesuite.bestpl.dto.UserScoreLog;
import com.jeesuite.bestpl.rest.dto.AddScoreParam;
import com.jeesuite.bestpl.rest.dto.PageQueryParam;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月25日
 */
@Component
@Singleton
@Path("/user_score")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
public class UserScoreController {
	
	@Autowired
	private IUserScoreService service;
	
	@POST
	@Path("add")
	public boolean addUserScore(AddScoreParam param){
		service.addScore(param.getUserId(),param.getScore(),param.getRemark());
		return true;
	}
	
	@POST
	@Path("page")
	public PageInfo<UserScoreLog> page(PageQueryParam param){
		return service.pageQuery(param.getPageNo(), param.getPageSize(), param.getUserId());
	}
	
	@GET
	@Path("user/{id}")
	public UserScore getByUser(@PathParam("id") int userId){
		return service.findUserScoreByUserId(userId);
	}
	
	

}
