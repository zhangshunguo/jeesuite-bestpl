### jeesuite-bestpl
基于jeesuite构建的一个完整项目,包含消息队列、缓存、定时任务、dubbo集成、springboot集成、restAPI发布。

### 依赖说明（如果出现包找不到或者某些class没找到）
- SNAPSHOT结尾的依赖包请自行下载[jeesuite-libs](http://git.oschina.net/vakinge/jeesuite-libs) 项目本地构建。
- release版的依赖请配置最高的版本号，版本号请看：[sonatype](https://oss.sonatype.org/content/repositories/releases/com/jeesuite/) 

### 模块说明
- bestpl-api 定义接口
- bestpl-dao 数据访问接口
- bestpl-service 服务实现
- bestpl-dubbo 基于dubbo服务发布
- bestpl-rest 基于jersey restAPI服务发布
- bestpl-web 基于springMVC服务发布
- bestpl-springboot 基于springboot服务发布

### 如何发布服务？
#### 默认是整体war包部署，构建完成后部署`bestpl-rest`或者`bestpl-web`下war包即可。
访问地址如：http://localhost:8080/index.html (加上你自己的contextpath)


#### 发布成dubbo服务
- producer端：构建完成后直接上传target下bestpl-app目录到服务器，通过bestpl-app下appserver.sh start启动
- consumer端：
  1. 去掉bestpl-rest的`bestpl-service`依赖，替换为：`bestpl-api`
  2. 在bestpl-rest依赖中增加dubbo依赖（参考bestpl-app的pom.xml）
  3. 用`dubbo-root.xml`内容替换`root.xml`
  4. 构建上传war包运行
 
 ### 如何启用配置中心
 1. 修改root.xml下对应配置remoteEnabled＝true，屏蔽本地配置代码
 2. 安装http://git.oschina.net/vakinge/jeesuite-admin
 3. 配置confcenter.properties
 #### 注意：启用配置中心请配置maven-resources-plugin打包时只包含confcenter.properties
 
 
### 基于[jeesuite-libs](http://git.oschina.net/vakinge/jeesuite-libs) 
### jeesuite统一管理平台[jeesuite-admin](http://git.oschina.net/vakinge/jeesuite-admin) 
 









