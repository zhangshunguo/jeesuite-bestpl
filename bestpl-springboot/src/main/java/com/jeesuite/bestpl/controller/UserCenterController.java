package com.jeesuite.bestpl.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jeesuite.bestpl.dto.UserScore;
import com.jeesuite.bestpl.dto.UserScoreLog;
import com.jeesuite.bestpl.dto.WrapperResponseEntity;
import com.jeesuite.bestpl.security.SecurityUtil;
import com.jeesuite.bestpl.service.UserScoreService;

@RestController
@RequestMapping("/api/ucenter")
public class UserCenterController {
	
	@Autowired
	private UserScoreService userScoreService;

	@RequestMapping(value = "myinfo", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity<Object>> getMyInfo(){
		return new ResponseEntity<WrapperResponseEntity<Object>>(new WrapperResponseEntity<Object>(SecurityUtil.getLoginUserInfo()),HttpStatus.OK);
	}
	
	@RequestMapping(value = "myscore", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity<Object>> getMyScore(){
		int userId = SecurityUtil.getLoginUserInfo().getId();
		UserScore userScore = userScoreService.findUserScoreByUserId(userId);
		return new ResponseEntity<WrapperResponseEntity<Object>>(new WrapperResponseEntity<Object>(userScore),HttpStatus.OK);
	}
	
	@RequestMapping(value = "myscore_logs", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity<Object>> getMyScoreLogs(@RequestParam("pageNo") int pageNo,@RequestParam("pageSize") int pageSize){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", SecurityUtil.getLoginUserInfo().getId());
		params.put("pageNo", pageNo);
		params.put("pageSize", pageSize);
		PageInfo<UserScoreLog> pageInfo = userScoreService.pageQuery(params);
		return new ResponseEntity<WrapperResponseEntity<Object>>(new WrapperResponseEntity<Object>(pageInfo),HttpStatus.OK);
	}

}
