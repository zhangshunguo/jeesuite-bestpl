/**
 * 
 */
package com.jeesuite.bestpl.dto.request;

import java.io.Serializable;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月13日
 */
public class LoginRequest implements Serializable {


	private static final long serialVersionUID = 1L;
	private String loginName;
	private String passwaord;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPasswaord() {
		return passwaord;
	}
	public void setPasswaord(String passwaord) {
		this.passwaord = passwaord;
	}
	
	
}
