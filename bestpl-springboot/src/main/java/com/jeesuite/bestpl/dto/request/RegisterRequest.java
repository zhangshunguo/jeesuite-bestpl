/**
 * 
 */
package com.jeesuite.bestpl.dto.request;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月12日
 */
 //@ApiModel(value = "AdjustSchemeDetailInput", description = "方案明细调整输入")
public class RegisterRequest {

	//@ApiModelProperty(value = "方案明细ID")
	private String name;

    private String password;

    private String mobile;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
    
    
}
