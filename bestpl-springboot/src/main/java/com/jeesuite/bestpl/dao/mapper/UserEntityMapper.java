package com.jeesuite.bestpl.dao.mapper;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.UserEntity;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;

import tk.mybatis.mapper.common.BaseMapper;

public interface UserEntityMapper extends BaseMapper<UserEntity> {
	
	@Cache
	UserEntity findByMobile(String mobile);
	
	@Cache
	UserEntity findByEmail(String email);
	
	@Cache
	@Select("SELECT * FROM users  where name=#{name} limit 1")
	@ResultMap("BaseResultMap")
	UserEntity findByName(String name);
}