package com.jeesuite.bestpl.task;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jeesuite.kafka.message.DefaultMessage;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;
import com.jeesuite.scheduler.AbstractJob;
import com.jeesuite.scheduler.JobContext;
import com.jeesuite.scheduler.annotation.ScheduleConf;

@Service
@ScheduleConf(cronExpr="0 */5 * * * ?",jobName="userScoreTask",executeOnStarted = true)
public class UserScoreTask extends AbstractJob {

	@Autowired
	private TopicProducerSpringProvider producerProvider;
	
	@Override
	public boolean parallelEnabled() {
		return false;
	}

	@Override
	public void doJob(JobContext context) throws Exception {
		producerProvider.publish("user_score_topic", new DefaultMessage("user_score_topic:"+UUID.randomUUID().toString()));
	}

}
