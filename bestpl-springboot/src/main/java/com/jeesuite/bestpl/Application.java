package com.jeesuite.bestpl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.jeesuite.springboot.starter.cache.EnableJeesuiteCache;
import com.jeesuite.springboot.starter.kafka.EnableJeesuiteKafkaProducer;
import com.jeesuite.springboot.starter.mybatis.EnableJeesuiteMybatis;
import com.jeesuite.springboot.starter.scheduler.EnableJeesuiteSchedule;

@SpringBootApplication
@MapperScan(basePackages = "com.jeesuite.bestpl.dao.mapper")
@EnableJeesuiteCache
@EnableJeesuiteMybatis
@EnableJeesuiteSchedule
@EnableJeesuiteKafkaProducer
@ComponentScan(value = {"com.jeesuite.bestpl"})
public class Application {
	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).web(true).run(args);
	}
	
	@Bean
    RestTemplate restTemplate() {
		return new RestTemplate();
    }
  
}
