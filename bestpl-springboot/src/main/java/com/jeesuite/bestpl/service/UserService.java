/**
 * 
 */
package com.jeesuite.bestpl.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.jeesuite.bestpl.dao.entity.UserEntity;
import com.jeesuite.bestpl.dao.mapper.UserEntityMapper;
import com.jeesuite.bestpl.dto.User;
import com.jeesuite.bestpl.dto.request.OpenIdBindRequest;
import com.jeesuite.bestpl.dto.request.RegisterRequest;
import com.jeesuite.bestpl.exception.JeesuiteBaseException;
import com.jeesuite.common.util.BeanCopyUtils;
import com.jeesuite.common.util.DigestUtils;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月25日
 */
@Service("userService")
public class UserService {

	/**
	 * 密码加密盐
	 */
	private static final String PASSWORD_SALT = "~w56!@#$%vg";

	@Autowired
	private UserEntityMapper userMapper;
	
	@Autowired
	private UserScoreService userScoreService;
	
	@Autowired
	private TransactionTemplate transactionTemplate;

	
	public User getUser(Integer userId) {
		UserEntity entity = userMapper.selectByPrimaryKey(userId);
		if(entity == null)throw new JeesuiteBaseException(1001,"用户不存在");
		return BeanCopyUtils.copy(entity, User.class);
	}

	
	@Transactional
	public User register(RegisterRequest request) {
		
		if(StringUtils.isBlank(request.getName())){
			throw new JeesuiteBaseException(1001, "用户名不能为空");
		}
		if(userMapper.findByName(request.getName()) != null){
			throw new JeesuiteBaseException(1001, "用户名已存在");
		}
		
		UserEntity entity = new UserEntity();
		entity.setName(request.getName());
		entity.setMobile(request.getMobile());
		entity.setPassword(DigestUtils.md5WithSalt(request.getPassword(), PASSWORD_SALT));
		entity.setRegisterAt(new Date());
		
		userMapper.insertSelective(entity);
		//新增积分
		userScoreService.addScore(entity.getId(), 100, "用户注册");
		
		return BeanCopyUtils.copy(entity, User.class);
	}

	
	public User registerByOpenId(OpenIdBindRequest request) {
        		
		return null;
	}

	
	public User login(String loginName, String password) {
		//TODO BY NAME,EMAIL
		UserEntity entity = userMapper.findByMobile(loginName);
		if(entity == null)throw new JeesuiteBaseException(1001,"用户不存在");
		if(!StringUtils.equals(DigestUtils.md5WithSalt(password, PASSWORD_SALT), entity.getPassword())){
			throw new JeesuiteBaseException(1002, "密码错误");
		}
		if(entity.getStatus() != 1){
			throw new JeesuiteBaseException(1001, "该账号已停用");
		}
		
		//新增积分
		userScoreService.addScore(entity.getId(), 5, "用户登录");
		
		return BeanCopyUtils.copy(entity, User.class);
	}

	
	

}
