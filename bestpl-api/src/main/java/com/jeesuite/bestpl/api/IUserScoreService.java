/**
 * 
 */
package com.jeesuite.bestpl.api;

import com.github.pagehelper.PageInfo;
import com.jeesuite.bestpl.dto.UserScore;
import com.jeesuite.bestpl.dto.UserScoreLog;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月25日
 */
public interface IUserScoreService {
	
	void addScore(int userId,int score,String remark);
	
	UserScore findUserScoreByUserId(int userid);
	
	PageInfo<UserScoreLog> pageQuery(int pageNo,int pageSize ,int userId);
}
