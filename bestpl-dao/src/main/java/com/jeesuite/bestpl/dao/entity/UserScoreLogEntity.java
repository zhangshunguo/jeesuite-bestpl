package com.jeesuite.bestpl.dao.entity;

import com.jeesuite.mybatis.core.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "user_score_logs")
public class UserScoreLogEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "user_score_id")
    private Integer userScoreId;

    @Column(name = "add_score")
    private Integer addScore;

    @Column(name = "current_score")
    private Integer currentScore;

    @Column(name = "last_score")
    private Integer lastScore;

    private String remarks;

    @Column(name = "created_at")
    private Date createdAt;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return user_score_id
     */
    public Integer getUserScoreId() {
        return userScoreId;
    }

    /**
     * @param userScoreId
     */
    public void setUserScoreId(Integer userScoreId) {
        this.userScoreId = userScoreId;
    }

    /**
     * @return add_score
     */
    public Integer getAddScore() {
        return addScore;
    }

    /**
     * @param addScore
     */
    public void setAddScore(Integer addScore) {
        this.addScore = addScore;
    }

    /**
     * @return current_score
     */
    public Integer getCurrentScore() {
        return currentScore;
    }

    /**
     * @param currentScore
     */
    public void setCurrentScore(Integer currentScore) {
        this.currentScore = currentScore;
    }

    /**
     * @return last_score
     */
    public Integer getLastScore() {
        return lastScore;
    }

    /**
     * @param lastScore
     */
    public void setLastScore(Integer lastScore) {
        this.lastScore = lastScore;
    }

    /**
     * @return remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

}