package com.jeesuite.bestpl.dao.entity;

import com.jeesuite.mybatis.core.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "user_score")
public class UserScoreEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id",updatable=false)
    private Integer userId;

    @Column(name = "current_score")
    private Integer currentScore;

    @Column(name = "last_score")
    private Integer lastScore;

    @Column(name = "updated_at")
    private Date updatedAt;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return current_score
     */
    public Integer getCurrentScore() {
        return currentScore;
    }

    /**
     * @param currentScore
     */
    public void setCurrentScore(Integer currentScore) {
        this.currentScore = currentScore;
    }

    /**
     * @return last_score
     */
    public Integer getLastScore() {
        return lastScore;
    }

    /**
     * @param lastScore
     */
    public void setLastScore(Integer lastScore) {
        this.lastScore = lastScore;
    }

    /**
     * @return updated_at
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}