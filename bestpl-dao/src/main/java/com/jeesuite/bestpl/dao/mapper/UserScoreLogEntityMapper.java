package com.jeesuite.bestpl.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.UserScoreLogEntity;

import tk.mybatis.mapper.common.BaseMapper;

public interface UserScoreLogEntityMapper extends BaseMapper<UserScoreLogEntity> {
	
	@Select("SELECT * FROM user_score_logs  where user_id=#{userId}")
	@ResultMap("BaseResultMap")
	List<UserScoreLogEntity> findByUsertId(int userId);
	
	@Select("SELECT * FROM user_score_logs  where user_id=#{userId} order by createdAt desc limit 1")
	@ResultMap("BaseResultMap")
	UserScoreLogEntity findLastByUsertId(int userId);
}