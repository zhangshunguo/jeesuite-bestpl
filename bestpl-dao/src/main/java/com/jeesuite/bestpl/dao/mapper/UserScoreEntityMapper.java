package com.jeesuite.bestpl.dao.mapper;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.UserScoreEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface UserScoreEntityMapper extends BaseMapper<UserScoreEntity> {
	
	@Select("SELECT * FROM user_score  where user_id=#{userId} limit 1")
	@ResultMap("BaseResultMap")
	UserScoreEntity findByUsertId(int userId);
}